package com.pandz.transactiondemo.controller;

import com.pandz.transactiondemo.dto.TransferRequest;
import com.pandz.transactiondemo.entity.Rekening;
import com.pandz.transactiondemo.service.RekeningService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/rekening")
public class RekeningController {
  @Autowired
  private RekeningService service;

  @PostMapping
  public Rekening create(@RequestBody Rekening request) {
    return service.create(request);
  }

  @GetMapping
  public Iterable<Rekening> findAll() {
    return service.findAll();
  }

  @PostMapping("/transfer")
  public void transfer(@RequestBody TransferRequest request) {
    String rekSource = request.getNoRekSource() != null ?  request.getNoRekSource() : "";
    String rekDestination = request.getNoRekDestination() != null ? request.getNoRekDestination() : "";
    double amount = !Double.isNaN(request.getAmount()) ? request.getAmount() : 0;

    System.out.println("\nSource: " + rekSource + "\nDestination: " + rekDestination + "\nAmount: " + amount);

    // Validate request
    if (rekSource.equals("") || rekDestination.equals("") || amount <= 0) {
      throw new RuntimeException("Bad Request");
    }

    service.transfer(rekSource, rekDestination, amount);
  }
}
