package com.pandz.transactiondemo.dto;

import lombok.Data;

@Data
public class TransferRequest {
  private String noRekSource;
  private String noRekDestination;
  private double amount;
}
