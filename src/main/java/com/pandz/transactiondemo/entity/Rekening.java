package com.pandz.transactiondemo.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name="rekening")
public class Rekening {
  @Id
  @GeneratedValue(strategy= GenerationType.IDENTITY)
  private Integer id;
  private String norek;
  private String nama;
  private double saldo;
}

//create database demotx;
//
//CREATE TABLE rekening (
//    id int not null auto_increment primary key,
//    norek varchar(25),
//    nama varchar(255),
//    saldo int(25)
//);