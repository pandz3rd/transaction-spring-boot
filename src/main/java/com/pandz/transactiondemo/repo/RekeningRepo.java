package com.pandz.transactiondemo.repo;

import com.pandz.transactiondemo.entity.Rekening;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RekeningRepo extends CrudRepository<Rekening, Long> {
  @Query(value = "SELECT * FROM rekening WHERE norek = ?1", nativeQuery = true)
  Rekening findByNoRek(String norek);
}