package com.pandz.transactiondemo.service;

import com.pandz.transactiondemo.entity.Rekening;
import com.pandz.transactiondemo.repo.RekeningRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class RekeningService {
  @Autowired
  private RekeningRepo rekeningRepo;

  public Rekening create(Rekening rekening) {
    return rekeningRepo.save(rekening);
  }

  public Iterable<Rekening> findAll() {
    return rekeningRepo.findAll();
  }

  @Transactional
  public void transfer(String source, String destination, Double amount) {
    // Validate rek source
    Rekening rekSource = rekeningRepo.findByNoRek(source);
    if (rekSource == null) {
      throw new RuntimeException("No rek source does not exist");
    }

    // Validate amount
    if (rekSource.getSaldo() <= amount) {
      throw new RuntimeException("Balance is not sufficient");
    }

    // update saldo
    rekSource.setSaldo(rekSource.getSaldo() - amount);
    rekeningRepo.save(rekSource);

    // Validate rek destination
    Rekening rekDestination = rekeningRepo.findByNoRek(destination);
    if (rekDestination == null) {
      throw new RuntimeException("No rek destination does not exist");
    }

    // update saldo
    rekDestination.setSaldo(rekDestination.getSaldo() + amount);
    rekeningRepo.save(rekDestination);
  }
}